#! /usr/bin/env python3
# -*- coding:Utf8 -*-

"""Setuptool script to build and install GaiaTools.

:author: Guillaume Plum <guillaume.plum@obspm.fr>
"""

# -------------------------------------------------------------------------------------------------
# All necessary import:
# -------------------------------------------------------------------------------------------------
from setuptools import find_packages, setup, Command
from sphinx.application import Sphinx


class Doctest(Command):
    """Command to run doctests."""

    description = 'Run doctests with Sphinx'
    user_options = []

    def initialize_options(self):
        """Inherited from Command."""
        pass

    def finalize_options(self):
        """Inherited from Command."""
        pass

    def run(self):
        """Inherited from Command."""
        sph = Sphinx(
            # source directory
            './doc/source',
            # directory containing conf.py
            './doc/source',
            # output directory
            './doc/build',
            # doctree directory
            './doc/build/doctrees',
            # finally, specify the doctest builder
            'doctest'
        )
        sph.build()


def get_requires():
    """Extract requirements from requirements.txt."""
    with open("requirements.txt", "r") as fich:
        return fich.readlines()


PACKAGES = find_packages(exclude="tests.*")

# -------------------------------------------------------------------------------------------------
# Call the setup function:
# -------------------------------------------------------------------------------------------------
setup(
    name='example',
    version='0.5a',
    description='Python Module for demonstrating testing capabilities.',
    author='Guillaume Plum',
    install_requires=get_requires(),
    packages=PACKAGES,
    cmdclass={
        'doctests': Doctest,
    },
    extras_require={
        'build_sphinx': ['sphinx>=1.3.1'],
    },
    test_suite='tests'
)

# vim:spelllang=
