# coding:utf-8


import numpy as np


def dummy_function(array: np.array) -> np.array:
    """Square the given :ref:`numpy.array`.

    Long description.

    >>> dummy_function(np.array([2, 3]))
    array([4, 9])

    :param array: An array
    :type array: numpy.array
    :return: the square of the array.
    :rtype: numpy.array
    """
    return array ** 2
