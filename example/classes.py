# coding: utf-8


class ClassMultiply:
    """A dummy class to test."""

    def __init__(self, value: int):
        """Constructor.

        :param value: Value to work with
        :type value: int
        """
        super(ClassMultiply, self).__init__()

        self._value: int = value

    def multiply(self, mul: int) -> str:
        """Multiply our internal by mul and convert it.

        :param mul: a multiplier.
        :type mul: int

        :return: the string formatted version of the results.
        :rtype: str
        """
        return "--{}--".format(self._value * mul)

    @property
    def value(self):
        """Return the internal value."""
        return self._value

    @value.setter
    def value(self, val):
        self._value = val

    @value.deleter
    def value(self):
        self._value = 0
