# Example project in python

[![pipeline status](https://gitlab.obspm.fr/gplum/python-example/badges/master/pipeline.svg)](https://gitlab.obspm.fr/gplum/python-example/commits/master)
[![coverage report](https://gitlab.obspm.fr/gplum/python-example/badges/master/coverage.svg)](https://gitlab.obspm.fr/gplum/python-example/commits/master)

For more information on the setuptools, look [here](https://setuptools.readthedocs.io/en/latest/setuptools.html)

## Files structure

```shell
.
├── doc
│   ├── make.bat
│   ├── Makefile
│   └── source
├── example
│   ├── classes.py
│   ├── func.py
│   └── __init__.py
├── README.md
├── requirements.txt
├── setup.py
└── tests
    ├── classes_test.py
    ├── func_test.py
    └── __init__.py

4 directories, 11 files
```

- `doc`: contain all documentation.
- `example`: module to deploy.
- `README.md`: a little read-me file (`rst` and `markdown` are the more common) to present the project.
- `requirements.txt`: all dependencies.
- `setup.py`: build instruction for the package.
- `tests`: all unit testing classes.

## Command to build
Building the project is very simple:

```bash
# Install all dependencies:
pip install -r requirements.txt
# Build the project artifact and pre-compile all python files:
python setup.py build
# this command can also install some dependencies if they are missing and declared.
```

## Command to build the documentation
Building the sphinx documentation is very simple:

```bash
cd doc
make html
```

## Command to test

To run test, I would advice against the `doctest` module, since integrating it into the `setuptools` is more complicated than planned.
To run the other test, here the unittest, you have to run:

```bash
python setup.py test
```

If you need a report, run them with:

```bash
coverage run --source example setup.py test
```

Then, to generate the report, you can use:
- for a `JUnit` compatible report:
```bash
coverage xml -o report.xml
```
- a console-based report:
```bash
coverage report -m
```
