# coding:utf-8

from unittest import TestCase

from example import ClassMultiply


class TestClassMultiply(TestCase):
    """A unittest class for ClassMultiply."""

    def test_getter(self):
        """Test the value getter."""
        instance = ClassMultiply(2)
        self.assertEqual(instance.value, 2)

    def test_setter(self):
        """Test the value setter."""
        instance = ClassMultiply(2)
        instance.value = 4
        self.assertEqual(instance.value, 4)

    def test_deleter(self):
        """Test the value deleter."""
        instance = ClassMultiply(2)
        del instance.value
        self.assertEqual(instance.value, 0)

    def test_multiply(self):
        """Test the :ref:`ClassMultiply.multiply` method."""
        instance = ClassMultiply(2)
        self.assertEqual(instance.multiply(2), "--4--")
