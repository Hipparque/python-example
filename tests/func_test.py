# coding:utf-8

from unittest import TestCase

import numpy as np

from example import dummy_function


class DummyFunctionTest(TestCase):
    """Test the dummy_function.

    The default of this unittest module is that we need a class,
    even for one method. But we could overcome this using a decorator.
    """

    def test_dummy_function(self):
        """Test the dummy_function."""
        test_data = np.array([2, 3, 2])
        res_data = dummy_function(test_data)
        expected = np.array([4, 9, 4])
        self.assertTrue((res_data == expected).all())
